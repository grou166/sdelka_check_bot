
import { Ctx, Hears, InjectBot, Start, Update } from 'nestjs-telegraf';
import { Context, Telegraf } from 'telegraf';
import { AppService } from './app.service';
import { actionButtons } from './app.buttons';


@Update()
export class AppController {

  constructor(
		@InjectBot() private readonly bot: Telegraf<Context>,
		private readonly appService: AppService
	) {
  }
check : boolean;


   async page_request(ctx:Context) {
   
    const axios = require('axios').default;
 await axios('https://backend.sdelkaservicedomain.ru/sdelka/health').
    then(() => {
  
    this.check = true;
    }) 
    .catch((err) => {
  this.check = false;
  if(typeof(err.response)!= 'undefined'){
 if(typeof(err.response.data.error) != 'undefined'){
for(let key in err.response.data.error){
  ctx.reply(`Микросервис : ${key} упал \nОшибка : ${err.response.data.error[`${key}`].message}`)
}
}
else ctx.reply(err.message);
} 
else ctx.reply(err.message);
})
this.check_request(this.check, ctx)
}
 async check_request(check,@Ctx()ctx: Context) {

    if(check === false){
      setTimeout(() =>{
    ctx.reply('Нажмите на кнопку после исправления',actionButtons());
      },1500)
  }

    if(check === true){
      setTimeout(async () => {
      this.page_request(ctx);       
    },120000);
    }
    
}

@Hears('Сервис исправлен⚒️')
async repeat(@Ctx() ctx: Context){
  this.page_request(ctx);
}

 @Start()
  async starter(ctx:Context){

    ctx.reply('Я запущен!')
    this.page_request(ctx);
   
  }

}