
import { Module } from '@nestjs/common';
import { TelegrafModule } from 'nestjs-telegraf';
import * as LocalSession from 'telegraf-session-local'
import { AppController } from './app.controller';
import { AppService } from './app.service';

const sessions = new LocalSession({ database: 'sesson_db.json' })

@Module({
	imports: [
		TelegrafModule.forRoot({
			middlewares: [sessions.middleware()],
			token:'5656313066:AAFrB2O46GdtzfU1eDHsLbm8bcpL2Jb13I8'
		}),
  ],
  providers: [AppService , AppController],
})

export class AppModule {}
